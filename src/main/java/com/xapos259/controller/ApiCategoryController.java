package com.xapos259.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos259.model.Category;
import com.xapos259.repository.CategoryRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ApiCategoryController {

	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> category = this.categoryRepository.findAll();
			return new ResponseEntity<>(category, HttpStatus.OK);
			
		} catch (Exception err) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	} 
	
	@PostMapping("category")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category) {
		Category categoryData = this.categoryRepository.save(category);
		if(categoryData.equals(category)) {
			return new ResponseEntity<>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
}
