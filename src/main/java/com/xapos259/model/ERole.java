package com.xapos259.model;

public enum ERole {

	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
	
}
